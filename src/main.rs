use std::io::Write;

use vm::interprit;

mod chunk;
mod debug;
mod value;
mod vm;

fn main() -> Result<(), ()> {
    // let mut chunk = Chunk::new();

    // let constant = chunk.add_constant(1.2);
    // chunk.add(Op::Constant(constant), 1);

    // let constant = chunk.add_constant(3.4);
    // chunk.add(Op::Constant(constant), 2);

    // chunk.add(Op::Add, 3);

    // let constant = chunk.add_constant(5.6);
    // chunk.add(Op::Constant(constant), 3);

    // chunk.add(Op::Divide, 3);
    // chunk.add(Op::Negate, 3);
    // chunk.add(Op::Return, 3);

    // chunk.dissassemle("Test chunk");
    // dbg!(interprit(chunk));

    let args = std::env::args().collect::<Vec<_>>();
    match args.len() {
        1 => repl(),
        2 => runfile(&args[1]),
        _ => {
            eprintln!("Usage: clox [path]");
            return Err(());
        }
    }
    Ok(())
}

fn repl() {
    let mut input = String::new();
    loop {
        print!("> ");
        std::io::stdout().lock().flush().unwrap();
        if std::io::stdin().read_line(&mut input).unwrap() == 0 {
            break;
        }
        interprit(&mut input);
        input.clear()
    }
}

fn runfile(file: &str) {
    let code = std::fs::read_to_string(file).unwrap();
    interprit(&code);
}

fn interprit(code: &str) {
    dbg!(code);
}
