use crate::chunk::{Chunk, Op};
use crate::value::{self, Value};

pub struct Vm {
    code: Chunk,
    ip: usize,
    stack: Vec<value::Value>,
}

pub fn interprit(c: Chunk) -> InterpretResult {
    let mut vm = Vm {
        code: c,
        ip: 0,
        stack: Vec::new(),
    };

    vm.run()
}

impl Vm {
    fn op(&mut self, f: impl Fn(Value, Value) -> Value) {
        let r = self.stack.pop().unwrap();
        let l = self.stack.pop().unwrap();
        let v = f(l, r);
        self.stack.push(v)
    }

    fn run(&mut self) -> InterpretResult {
        loop {
            let op = self.code.ops[self.ip];
            dbg!((op, &self.stack, &self.code.constants));
            match self.code.ops[self.ip] {
                Op::Return => {
                    dbg!(self.stack.pop());
                    return InterpretResult::Ok;
                }
                Op::Constant(idx) => {
                    let constant = self.code.constants[idx];
                    self.stack.push(constant)
                }

                Op::Negate => {
                    let val = -self.stack.pop().unwrap();
                    self.stack.push(val)
                }
                Op::Add => self.op(|x, y| x + y),
                Op::Subtract => self.op(|x, y| x - y),
                Op::Multiply => self.op(|x, y| x * y),
                Op::Divide => self.op(|x, y| x / y),
            }
            self.ip += 1;
        }
    }
}

#[derive(Debug)]
pub enum InterpretResult {
    Ok,
    // CompileErr,
    // RuntimeErr,
}
