use crate::chunk::{Chunk, Op};

impl Chunk {
    pub fn dissassemle(&self, name: &str) {
        eprintln!("=== {} ===", name);
        let mut last_line = None;
        for (opnum, (opcode, lineno)) in self.ops.iter().zip(self.lines.iter()).enumerate() {
            eprint!("{:04} ", opnum);

            if last_line == Some(lineno) {
                eprint!("   | ");
            } else {
                eprint!("{:>4} ", lineno);
            }

            fn simple(x: &str) {
                eprintln!("{:<16}", x);
            }

            match opcode {
                Op::Return => simple("RETURN"),
                Op::Negate => simple("NEGATE"),
                Op::Add => simple("ADD"),
                Op::Divide => simple("DIVIDE"),
                Op::Multiply => simple("MULTIPLY"),
                Op::Subtract => simple("SUBTRACT"),
                Op::Constant(idx) => {
                    eprintln!("CONSTANT · {:<16?} ({})", self.constants[*idx], *idx)
                }
            }

            last_line = Some(lineno);
        }
    }
}
