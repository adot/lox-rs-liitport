use crate::value;

#[derive(Debug, Clone, Copy)]
pub enum Op {
    Return,
    // Index into the constant pool
    Constant(usize),
    Negate,
    Add,
    Subtract,
    Divide,
    Multiply,
}

#[derive(Default)]
pub struct Chunk {
    pub ops: Vec<Op>,
    pub constants: value::ValueArray,
    pub lines: Vec<u32>,
}

impl Chunk {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn add(&mut self, o: Op, line: u32) {
        self.ops.push(o);
        self.lines.push(line);
    }

    pub fn add_constant(&mut self, v: value::Value) -> usize {
        self.constants.push(v);
        self.constants.len() - 1
    }
}

// pub type Chunk = Vec<OpCode>;
